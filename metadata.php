<?php
$sMetadataVersion = '2.1';
$aModule = array(
    'id' => 'dre_scroll',
    'title' => '<img src="../modules/bender/dre_scroll/out/img/favicon.ico" >odynova Infinity Scrolling',
    'description' => 'Artikellisten ohne Pageination, infinity scroll',
    'thumbnail' => 'out/img/logo_bodynova.png',
    'version' => '0.0.3',
    'author' => 'André Bender',
    'url' => 'https://bodynova.de',
    'email' => 'a.bender@bodynova.de',
    'extend' => array(
        \OxidEsales\Eshop\Core\ViewConfig::class =>
            \Bender\Scrolling\Core\ViewConfig::class,
    ),
    'blocks' => array(
        array(
            'template' => 'layout/base.tpl',
            'block' => 'base_js',
            'file' => 'base_js.tpl'
        ),
        array(
            'template' => 'layout/base.tpl',
            'block' => 'base_style',
            'file' => 'base_style.tpl'
        )
    ),
);
