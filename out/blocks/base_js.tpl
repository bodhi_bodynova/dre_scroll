[{$smarty.block.parent}]
[{if $oView->getClassName() eq "alist" || $oView->getClassName() eq "search" || $oView->getClassName() eq "manufacturerlist" }]
    [{assign var="pages" value=$oView->getPageNavigationLimitedBottom() }]

	[{* Infinity Scroll APi-Doc: https://infinite-scroll.com/ *}]
    [{capture assign=infiniteScroll}]
    $(function(){
        var productlistselector = '#' + $("div[id*='myListID']").attr('id');
        $(productlistselector).infiniteScroll({

	        debug: true,
            /*
	        navSelector  : "div.refineParams.bottomParams",
            // selector for the paged navigation (it will be hidden)
            nextSelector : "ol.pagination.pagination-sm>li.next>a",
            // selector for the NEXT link (to page 2)
            itemSelector : productlistselector + " div.productData",
            // selector for all items you'll retrieve
	        */
	        append: productlistselector + " div.productData",
            [{assign var=fp value='&amp;'|str_replace:'&':$pages->firstpage}]
            [{assign var=lp value='&amp;'|str_replace:'&':$pages->lastpage}]
	        path:'[{$fp}]{{#}}',
	        checkLastPage: 'ol.pagination.pagination-sm>li.next>a',
	        hideNav: 'div.refineParams.bottomParams',
	        status: '.page-load-status',
	        request: '',
	        load:'',
	        last:'',
	        error:''
	    });




	    /*
	    let infScroll = $('productlistselector').data('infiniteScroll');
	    console.log(`Infinite scroll at page '+ ${infScroll.pageIndex});


        var scrollbutton = $('<a id="scrolltop" class="submitButton largeButton" style="position:fixed; bottom:30px; display:none;"><span class="asc">Nach oben</span></a>');
		$('body').append(scrollbutton); 

		$(window).scroll(function(e){
			var scrollTop = $(window).scrollTop();
			if(scrollTop > 800){
				alignScrolltopButton();
				scrollbutton.show();
			}else{
				scrollbutton.hide();
			}
		});


		$(window).resize(function(e){
			alignScrolltopButton();
		})

		scrollbutton.click(function(){
			$(window).scrollTop(0);
			return false;
		});

		alignScrolltopButton();

		function alignScrolltopButton(){

	        var pos = $('#left').offset();
			var left = pos.left;
			scrollbutton.css('left', left+'px');

			var pos = $('#right').offset();
			var maxTop = pos.top + $('#right').height();
			var top = $(window).height() - 30 + $(window).scrollTop();
			if(top > maxTop){
				scrollbutton.css('position', 'absolute');
				scrollbutton.css('bottom','');
				scrollbutton.css('top', maxTop);
			}else{
				scrollbutton.css('position', 'fixed');
				scrollbutton.css('bottom','30px');
				scrollbutton.css('top', '');
			}

			
		}
	    */

    })
    [{/capture}]
	[{* Old *}]
    [{*oxscript include=$oViewConf->getModuleUrl('dre_scroll','out/src/jquery.infinitescroll.min.js') *}]
	[{* Dev *}]
    [{oxscript include=$oViewConf->getModuleUrl('dre_scroll','out/src/infinite-scroll.pkgd.js') }]
	[{* Live *}]
    [{*oxscript include=$oViewConf->getModuleUrl('dre_scroll','out/src/infinite-scroll.pkgd.min.js') *}]
    [{oxscript add=$infiniteScroll}]
[{/if}]
